package controllers

import javax.inject._

import exceptions.{IncorrectTokenException, NotFoundException, RateLimitExceededException}
import play.api.libs.json.{JsArray, JsString, Json}
import play.api.libs.ws.WSClient
import play.api.mvc._
import services.DribbleService
import play.api.cache.Cached

import scala.concurrent.{ExecutionContext, Future}


class DribbleController @Inject()(ws: WSClient, dribbleService: DribbleService, cached: Cached)(implicit exec: ExecutionContext) extends Controller {

  def top10(user: String) = cached.status(_ => user, 200, 3600) {
    Action.async {
      dribbleService.followers(user).flatMap{followers =>
        val likers: Seq[Future[Seq[String]]] = followers.map{ name =>
          dribbleService.likers(name)
        }
        val flattenedLikers: Future[Seq[String]] = Future.sequence(likers).map(_.flatten)
        val likersWithLikeCount = flattenedLikers.map(names => names.groupBy(identity).mapValues(_.size))
        dribbleService.topLikers(likersWithLikeCount, 10)
      }.map(names => Ok(JsArray(names.map(name => JsString(name)))))
        .recover{
          case e: RateLimitExceededException => TooManyRequests(Json.obj("msg" -> "Too many requests, please try later"))
          case e: IncorrectTokenException => ExpectationFailed(Json.obj("msg" -> "Incorrect token for Dribble API"))
          case e: NotFoundException => NotFound(Json.obj("msg" -> "Dribble returned 404 error"))
          case e: Throwable => InternalServerError(Json.obj("msg" -> e.getMessage))
        }

    }
  }

}
