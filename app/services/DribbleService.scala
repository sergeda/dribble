package services

import javax.inject.Inject

import exceptions.{IncorrectTokenException, NoTokenException, NotFoundException, RateLimitExceededException}
import play.api.Configuration
import play.api.libs.json.JsValue
import play.api.libs.ws.{WSClient, WSRequest, WSResponse}

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._
import scala.collection.immutable.ListMap


/**
  * Class makes all calls to Dribble API
  */
class DribbleService @Inject()(ws: WSClient, configuration: Configuration)(implicit exec: ExecutionContext){
  val token = configuration.getString("dribble.token").getOrElse(throw new NoTokenException)

  /**
    * Forms request
    */
  private def request(url: String): WSRequest = ws.url(url)
    .withHeaders("Authorization" -> s"Bearer $token")
    .withRequestTimeout(60.seconds)

  /**
    * Returns url to get followers by user
    */
  private def followersUrl(username: String, json: Future[JsValue]): Future[String] =
    json.map(js => (js \ "followers_url").as[String])

  /**
    * Returns url to get shots by user
    */
  private def shotsUrl(username: String, json: Future[JsValue]): Future[String] =
    json.map(js => (js \ "shots_url").as[String])

  /**
    * Returns sequence of urls to get likers for each shot
    */
  private def likeUrls(json: Future[JsValue]): Future[Seq[String]] =
    json.map(js => (js \\ "likes_url").map(_.as[String]))

  /**
    * Returns sequence of user names from input json
    */
  private def userNames(json: JsValue): Seq[String] =
    (json \\ "username").map(_.as[String])

  /**
    * Throws respective exception if status code of request result is not 200
    */
  private def checkStatus(statusCode: Int) = statusCode match {
    case 200 =>
    case 429 => throw new RateLimitExceededException
    case 401 => throw new IncorrectTokenException
    case 404 => throw new NotFoundException
    case _ => throw new Exception("Dribble api returned error status")
  }

  /**
    * Returns json with all the data for user
    */
  private def userData(username: String): Future[JsValue] = {
    val req = request(s"https://api.dribbble.com/v1/users/$username")
    req.get.map { response =>
      checkStatus(response.status)
      response.json
    }
  }

  /**
    * Returns sequence of follower's names for user
    */
  def followers(username: String): Future[Seq[String]] = {
    followersUrl(username, userData(username)).flatMap{url =>
      val req = request(url)
      req.get.map { response =>
        checkStatus(response.status)
        userNames(response.json)
      }
    }
  }


  /**
    * Returns list of user shots in json format
    */
  private def shots(username: String): Future[JsValue] = {
    shotsUrl(username, userData(username)).flatMap{url =>
      val req = request(url)
      req.get.map { response =>
        checkStatus(response.status)
        response.json
      }
    }
  }

  /**
    * Returns list of likers names with likes count
    */
  def likers(username: String): Future[Seq[String]] = {
    likeUrls(shots(username)).flatMap{ urls =>
      val requests: Seq[WSRequest] = urls.map(request)
      val responses: Seq[Future[WSResponse]] = requests.map(r => r.get())
      val res = responses.map{ f =>
        f.map{ resp =>
          checkStatus(resp.status)
          userNames(resp.json)
        }
      }
      Future.sequence(res).map(_.flatten)
    }
  }

  /**
    * Returns list of top likers names
    */
  def topLikers(likes: Future[Map[String, Int]], howMany: Int): Future[Seq[String]] = {
    likes.map(ls => ls.toSeq.sortWith((a, b)=> a._2 >b._2).take(howMany).map(_._1))
  }


}
