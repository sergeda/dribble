package exceptions

/**
  * Indicates that we used all the available requests for Dribble API.
  */
class RateLimitExceededException extends Exception