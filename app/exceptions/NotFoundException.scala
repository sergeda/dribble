package exceptions

/**
  * Indicates that 404 error was returned by Dribble
  */
class NotFoundException extends Exception
