package exceptions

/**
  * Indicates that Dribble doesn't accept our token
  */
class IncorrectTokenException extends Exception
