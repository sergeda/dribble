package exceptions

/**
  * Indicates that there is no token for Dribble in application configuration
  */
class NoTokenException extends Exception
