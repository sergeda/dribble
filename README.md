Solution to Dribble task using Play application
=================================


Task description
===========

You have to create tool to calculate Dribbble stats using dribble public api:
1. For given Dribbble user find all followers
2. For each follower find all shots
3. For each shot find all "likers"
4. Calculate Top10 «likers». People with greater like count descending.
Implement an api endpoint where user login is a parameter. Ex. http://0.0.0.0:9000/top10?login=alagoon
Output the results as json.
Think about:
How to deal with Dribbble API Limits? Try solve this problem in your solution.


### How to run application

To run this application from the console using sbt.
For example: "sbt run"
After server started you can open url http://127.0.0.1:9000/top10/[LOGIN OF USER]
(Replace [LOGIN OF USER] with existing Dribble user)